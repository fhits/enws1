---
author: Andreas Murk | Edmond Simzian
title: English definitions 
---

# Definitions

### IDE 

Integrated development environment can be described as the workbench of a programmer. With integrated tools like syntax highlighting, different debugger, refactor methods and more, it is used to write code in an easier way.

### Programming language

There are several programming languages used for various fields. Most commonly used languages are based on C. Apart from that there are different levels of programming languages. C is rather a lower-based language than Java as of its lower functionalities like memory allocation. Java is more abstracted using Object Orientation instead of using pointers to reference your values and associated adresses.

### Includes (Library)

``#include <stdlib.h>``

Commands and functions are defined in libraries which are included at the beginning of the source-code. When it is being compiled, the content of the header files will be copied into your code. Mostly, libraries are written in header files > **stdlib.h**

# 

#####  

### Main function

```
int main(void) {
	return EXIT_SUCCESS;
}

int main(int argc, char const *argv[])
{
    return EXIT_SUCCESS;
}
```

The main function initializes the start sequence of your code. Here is where your programm will start from. The return type integer indicates that an integer must be returned. ``return 0`` shows an successful end of all written statements. Whenever the code was not successful, ``return 1`` has been returned. You can also use ``return EXIT_SUCCESS`` and ``return EXIT_FAILURE`` statements from ``stdlib.h`` for more clear endings of the code. 

### Jumps

```
void print_output(char* message) {
	printf("%s", message);
}

int main(void) {
	char* message = "Message in here";	
	print_output(message);
	return EXIT_SUCCESS;
}
```

The code starts normally in the main function. We declare a variable `message` which stores a string. The next statement calls the function `print_output` and then **jumps** to the declaration of this specific function.
In the earlier stage of programming, jumps were realized using **goto** statements. As they are not common anymore, you would substitute each jump which a function call or other type of structure (loops or sequences).

### Array 

```
int[5] array = {1, 2, 3, 4, 5, 6};
```

An array is a data structure, which contains several elements of ONE data type. When intializing, it is needed to specify how much space it needs one way or the other. Starting with an index of 0 to n where n is the length of the array. When writing algorithms, you mostly need to take advantage of using arrays as they hold many values in just one variable.

### Define

```
#define MAX 5
```

You can define a constant value to use it in your code. The constant cannot be changed unless you redefine it. Furthermore, you are able to define functions. When the linker then links all source files together, it only needs to be defined once in all of your source code files.

### String 

```
char[5] array = {'H', 'e', 'l', 'l', 'o', '\0'};
```

A string is a collection of chars. Normally, the `\0` indicates  the end of a string. For text files you often need to check against `EOF` (End of File) to be aware, when the text is has ended and no more processing of this text is needed. Strings are mostly used when you process text files and need to properly print it out.

### Syntax

Syntax is the language style. Or the grammar of a programming langauge if you like. In Python for example there are no curly brackets **{}** and semicolons **;** as in other languages. You will simply indent your code to visualize the scopes of functions or sequences:

```
for i in range(0, 10):
	print(i)
```

### Datastructures

Datastructures like arrays, queues, lists, binary trees etc. are used for various tasks. But as the name suggests, it structures information. In C you are able to define own data structures using the `struct` keyword.

```
struct Human {
	int weight;
	int height;
	char* name;
}
```

### Pointers

Every variable is a memory location and this location needs to be find out. Otherwise, you are not able to alter this value on that specific address. Pointers are tools which store the address of other variables - the direct memory location . For example:

```
int main(void) {
	char[10] own_string = "Hello";
	char* pointer = &own_string;
}
```

This pointer now **points** to the address of the declared array above (with `&` you can pass in the **address** of a variable). Pointers always points to the first address of arrays. This means that at the beginning the pointer knows the first value of the array **own_string** which is `"H"`. Later, it can then be used to retrieve more values from this array.

### Control Structures

### Repetition structures:

**For loop** Used to specify an iteration to execute code repeatedly.

```
for(int i = 0; i < 10; i++) {
	printf("%d", i);
}
```

**While** A while loop requires a condition. For example DO THAT as long CONDITION1 applies. You can also make an endless loop -> while(1) or while(true) when using boolean values with stdbool.h. 

```
while (value == true) {
	printf("%d", somevalue);
}
```

**Do-while** - The only difference between a **while** and **do-while** is that it will at least run once because you check the condition after the first iteration. In a **while** loop it can be the case that it will be not executed at all:

```
do {
	printf("%d", somevalue);
}
	(while (value == true);
```

### Sequence structures:

**If-then** Whenever the value is true then do task 1. For example, the programm asks the user to input his age. If (age > 18) print out the age:

```
if(age > 18) {
	printf("%d", age);
}
```

**If-else** IF CONDITION1 true then DO TASK1, IF CONDITION1 false then DO TASK2. This is used to prevent the programm to verify every if statement. If I write 10 If statements in a row, the programm will read every 10 even if the correct statement is already found and ready to be executed.

```
if(age > 18) {
	printf("%d", age);
} else {
	printf("pupil is older than 18");
}
```

**Switch case** If you expect more than one outcome from an operation, you can manage those with a switch case. You can then describe what each output should execute.

```
switch(age) {
	case 17:
		printf("the pupil is underage");
	break;
	case 18:
		printf("the pupil is full-aged");
	break;
	default:
	break;
}
```

### Return values

```
int my_calculation(int x, int y) {
	return x + y;
}

int main(void) {
	int number1 = 5;
	int number2 = 10;
	printf("%d", my_calculation(number1, number2));
}
```

Every function has a return type. **void** is a return type which gives us nothing (void). In this case, the function `my_calculation` takes two numbers as parameters and returns an integer. These two values will be then added and returned (as an integer). Afterwards, we are able to retrieve that value from the return type of the invoked function in the main function and print out the returned value. Return types are often convenient when you need to process data and then work with the altered values. Instead of declaring and calculate variables within one function, it can be separated in different functions to abstract the logic of your code into single units (functions).

### Recursion

```
int rec(int number) {
	if (number > 1) {
		rec(number -1);
	}
}

int main(void) {
	int number = 10;
	rec(number);
}
```

Recursion is when you call the function in the same function again. On each call the stack will push the current invocation onto its stack. When the number now is 0, the recursion is ended all it will return all values from the latest call to the first call (in recursive order). This is realized as the stack always uses LIFO (last in first out).

### Operators

#####Logic &&

Logic AND links  CONDITIONS. If both are true, a declared COMMAND is executed. When comparing binary numbers only when both are 1 it gives 1 and is therefore true.

```
if(age > 17 && age <= 25) {
	printf("pupil is able to buy a ÖBB youth card");
}
```

#####Logic ||

Logic OR links  CONDITIONS. If one of them are true, a declared COMMAND is executed. When comparing binary numbers and one of them is 1. Both are 1 and therefore true.

```
if(pupil == drunk || pupil == tired) {
	printf("pupil can cause a car accident");
}
```

### Calculations

##### < | > Greater and lesser

E. g. return true if (value1 > value2) $\rightarrow$ is greater than value2. And same for **<**

##### & Adress operator 

Assign the address of **x** to a pointer **p**. If you use `scanf` to get input from a user, you need to assign given input to an address. You do that with `&`.

##### XOR 

Exclusive OR is a bitwise operator. When two bits are identical, XOR coughs up a 0. When the two bits are different, XOR spits out a 1. You can use it to encrypt text for example. Linking the text and a peronal key in their binary form with XOR, will give you an encrypted text.

##### % Modulo operator

divide VALUE % VALUE and get the remainder. For example: 40%13 = 1. It is also a nice way to write a program which converts dezimal numbers into binary.

```
#include <stdlib.h>
#include <stdio.h>

int binary(int number)
{
    if (number == 0)
    {
        return 0;
    }
    else
    {
        return (number % 2 + 10 * binary(number / 2));
    }
}

int main(int argc, char const *argv[])
{
    int dec_number = 4;
    printf("%d", binary(dec_number)); // Output: 100
    return 0;
}
```

### Referencing, Dereferencing

Whenever we store an address from a variable in a pointer, we often want to process the value associated with this address.

```
int main(void) {
	int a = 5; // variable a has value 5
	int* pointer = &a; // pointer points to address from variable a
	printf("%d", *pointer);
}
```

When we want to print out the value which is is stored in the variable **a**, we are not able to do this with the pointer itself. It would simply return the address in a hexadecimal format. Instead, we need to **dereference** the address and therefore retrieve the value which is stored in the address of variable **a**.

### Functions 

A function is a statement or a group of statements. The main function is declared in every code. You can also write functions outside the source code and call it in the sourcecode to execute it. For example, you write a function which executes a switch-case outside of the main function and then only call it with its name in the main function to use it.

### Parameters 

Parameters are the arguments we pass in a function. We write them in the declaration within the parentheses following the function. When we then call the function, we need to pass in values for this declared parameters:

```
void print_number(int number) {
	printf("%d", number);
}

int main(void) {
	int some_number = 100;
	print_number(100);
	return 0;
}
```

### Data types

There are different data types which take up different amount of space or depict other than values. An Integer for example, shows only whole numbers from -2^16 to +2^16. With float, you can depict floating point numbers. If you need bigger numbers than an integer, you can use double for example. To show letters, we use char for characters.
Int float char - Integer depicts whole numbers, float shows floating point numbers and char can hold letters.

### Global / Local

A global variable is defined outside of all functions and can therefore be used for every function. A local variable can only be used withing the function scope where it has been declared.

```
int global = 5;

void print_output() {
	printf("%d", global);
}

int main(void) {
	int local = 10;
	printf("%d", local);
	printf("%d", global);
}
```

This global declared variable **global** can be used within every function and the scope would be the whole file instead of the block of the declaration itself. Instead, the local variable can only be used within the specified main function. Variables are stored on the call stack of every function but global variables are stored in special data segments requested by the operating system. Therefore, they are existing of the lifetime of the process (the execution of this programm).

### Delimiters

A delimiter is a unique character from the programming language which indicates the beginning or end of a statement, string or function body.

```
void my_function(int number) {
	printf("%d", number);
} 
```

The **{** indicates that the function body will start here. The closing bracket **}** then declares the end of the function **my_function**.

### Declaration

Whenever you **declare** a variable, you simply give them names and a specified data type to work with them. Otherwise, you would not have any methods to retrieve that specific value of this variable.
``int test;`` means that the variable has the name **test** and the data type **int**.

### Assignment

Every declared variable needs a value to process them. When you think about calculations, you always need to have values to work with. ``3 + 3 = 6`` $\rightarrow$ you need both values **3** and **3** to calculate the sum out of those.
``int test = 20;`` This indicates that the variable which has been previously **declared** as **test** now stores the value 20 in it. Afterwards you are able to process this variable with calculations or other nice things.

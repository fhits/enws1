#include <stdio.h>
#include <stdlib.h>

// Added type definition for better type display
typedef struct head
{
    int data;
    struct head *next;
} Node;

int empty(Node *head);
//CHANGE: renamed parameter name
//CHANGE: update actual pointer to new head instead of returning it (function arguments are passed by value -> only copy of parameter will be changed instead of actual pointer)
void push(Node **head, int data)
{
    // Create temp head pointer for holding values
    Node *tmp = (Node *)malloc(sizeof(Node));
    if (tmp == NULL) exit(0);
    // Set temp data to actual pushed value
    tmp->data = data;
    // Set next head pointer to actual head (initially NULL)
    tmp->next = *head;
    // Set initial NULL head to current tmp which holds values
    *head = tmp;
}
//CHANGE: renamed parameter name
void pop(Node **head)
{
    if (empty(*head))
    {
        printf("Cannot pop with empty Stack!\n");
    }
    else
    {
        // Set tmp to head for iterating
        Node *tmp = *head;

        printf("Popping value %d\n", tmp->data);
        // Set head to next value (popped value is no longer existing)
        *head = (*head)->next;

        // free tmp pointer
        free(tmp);
    }
}

//CHANGE: renamed parameter name
int empty(Node *head)
{
    // Check if head is NULL (initially) or contains a pointer
    return head == NULL ? 1 : 0;
}

//CHANGE: renamed parameter name
void display(Node *head)
{
    //CHANGE: Use function to verify if stack is empty
    if (empty(head))
    {
        printf("Stack is empty!\n");
        return;
    }

    // Iterates trough all heads starting by head and prints out values (->data)
    Node *current = head;
    if (current != NULL)
    {
        printf("Stack: ");
        do
        {
            printf("%d ", current->data);
            current = current->next;
        } while (current != NULL);
        printf("\n");
    }
}

int main(int argc, char const *argv[])
{
    Node *head = NULL;
    display(head);
    push(&head, 15);
    display(head);
    push(&head, 22);
    display(head);
    pop(&head);
    display(head);
    pop(&head);
    display(head);
    pop(&head);
    return EXIT_SUCCESS;
}

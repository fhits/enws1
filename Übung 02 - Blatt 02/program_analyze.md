---
author: Fabian Untermoser | Andreas Murk
title: Stack Program Analyze
description: Explanation of a basic stack implementation
---

This program represents a basic stack implementation. A stack is a conceptual data structure mainly used in programming and informatics which uses the **LIFO** (Last In First Out) procedure.
This means that the elements can be retrieved only in the order that they were inserted.
The element which was added last is the first to be removed, and so on.

![Flow chart](img/stack-flowchart.png)

#
#
#


The program is structured into several functions:


# **push**:
The push function creates a temporary pointer with size of the *Node* struct type.
If the *malloc* operation fails, the program terminates.
Next, the passed in data is set to the temporary Node.
The head will be assigned to this temporary pointer.
Afterwards, the node called *head* is updated.

![IPO Table for function push](img/stack_1.png)

```C
void push(Node **head, int data)
{
    Node *tmp = (Node *)malloc(sizeof(Node));
    if (tmp == NULL) exit(0);

    tmp->data = data;
    tmp->next = *head;

    *head = tmp;
}

```

#

# **pop**:
Within the pop function, the head represents the Node to be popped.
It first checks if the given Node is empty.
Only if the Node is not null, the *pop* function proceeds.
Furthermore, a temporary pointer is created for holding the current head pointer.
Then the head is assigned to its next Node and the temporary variable is released to avoid memory leaks.

![IPO Table for function pop](img/stack_2.png)

```C
void pop(Node **head)
{
    if (empty(*head))
    {
        printf("Cannot pop with empty Stack!\n");
    }
    else
    {
        Node *tmp = *head;

        printf("Popping value %d\n", tmp->data);
        *head = (*head)->next;

        free(tmp);
    }
}

```
#
#
#
# **display**:
This function iterates trough all existing Nodes (starting at given Node *head*) until no more pointers exist.
It will then print out the data, if the Node exists.

![IPO Table for function display](img/stack_3.png)

```C
void display(Node *head)
{
    if (empty(head))
    {
        printf("Stack is empty!\n");
        return;
    }

    Node *current = head;
    if (current != NULL)
    {
        printf("Stack: ");
        do
        {
            printf("%d ", current->data);
            current = current->next;
        } while (current != NULL);
        printf("\n");
    }
}
```
#
#
# **empty**:
This function is used to verify if the stack is empty or not.
It does so by checking against NULL.

![Empty IPO Table](img/stack_4.png)

```C
int empty(Node *head)
{
    return head == NULL ? 1 : 0;
}
```

# Results

In our refactor process we have improved the readiblity of the code by using a typedefinition.
Furthermore, we have removed the return value in two functions *pop* and *push*.
Instead, we update the provided Node parameter to its new state.
Now instead of working with the return value:

```C
...
    head = push(head, 15);
    head = pop(head);
...
```

We only need to pass in the address of the pointer now:

```C
...
    push(&head, 15);
    pop(&head);
...
```